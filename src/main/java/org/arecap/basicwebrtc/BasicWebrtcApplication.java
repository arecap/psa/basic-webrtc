package org.arecap.basicwebrtc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BasicWebrtcApplication {

	public static void main(String[] args) {
		SpringApplication.run(BasicWebrtcApplication.class, args);
	}

}
